import React,{useState} from 'react';
import Bootstrap from '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './Login.css'
import swal from 'sweetalert'

function Login(){

    const[username,setusername]=useState('')
    const[password,setpassword]=useState('')

    function validate(event){
        event.preventDefault();
        if(username=='anand' && password=="anand123"){
            swal("Login Successfull","Congraz..!",'success')
        }
        else(
            swal("Login Failed","Try Again",'error')
        )
    }


    return <div>

        <div className="row justify-content-center">
            <div className='col-md-4'>
                <h2>User Authentication</h2>

             <form onSubmit={validate}>
                <input type="text" placeholder='username' className='form-control' value={username}
                onChange={(e)=>{setusername(e.target.value)}} />
                <input type="password" placeholder='password' className='form-control' value={password}
                  onChange={(e)=>{setpassword(e.target.value)}} />
                <input type="Submit"  value="Login" className="btn btn-primary"/>

             </form>
            </div>

        </div>

    </div>
}
export default Login